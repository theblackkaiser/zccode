import Vue from 'vue'
import App from './App'
import appNav from './pages/components/app-nav.vue'

Vue.config.productionTip = false
Vue.component('appNav',appNav)

App.mpType = 'app'

const app = new Vue({
	...App
})
app.$mount()
